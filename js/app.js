class PresentationCardMosu extends HTMLElement {
    
    constructor() {
        super();
    }

    connectedCallback(){
        //console.log("agregado");
        const shadow = this.attachShadow({mode: 'open'});
        let div = document.createElement('div');
        div.id = 'container';
        shadow.appendChild(div);

        this.showMessage(this);
    }

    disconnectedCallback(){
        console.log("removido")
    }

    static get observedAttributes() {
        return ['name', 'last-name', 'email'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        console.log(`Propiedad ${name} was changed from ${oldValue} to ${newValue}`);
        if (oldValue !== null) {
            //como alguno de los atributos que observo cambiaron, mando a llamar a la funcion para verificar
            this.showMessage(this);
        }
    }

    showMessage(element){
        //element.shadowRoot.childNodes[0] ===> me devuelve el div
        let name = element.getAttribute('name');
        let lastName = element.getAttribute('last-name');
        let email = element.getAttribute('email');
        let msg = '';
        if (name.toLowerCase() === 'manuel') {
            msg += 'Soy yo!!! , ';
            msg += ` Mi correo es ${email}`;
            if (this.isExistGmailInString(email)) {
                msg += ' ==> Mi correo pertenece a Skynet :3';
            }
        } else { 
            msg += `Hola ${name} ${lastName} , `;
            msg += ` Tu correo es ${email} `
            if (this.isExistGmailInString(email)) {
                msg += ' ===> Eres parte de Skynet :3';
            }
        }
        element.shadowRoot.childNodes[0].innerHTML = msg;
    }

    isExistGmailInString(str) {
        return str.includes('gmail'); 
    }

}

customElements.define('presentation-card-mosu', PresentationCardMosu);